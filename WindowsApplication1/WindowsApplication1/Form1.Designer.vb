﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.lstvAvailableNetworkList = New System.Windows.Forms.ListView()
        Me.ColumnHeader1 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader2 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader3 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader4 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader5 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader6 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader7 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader8 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader9 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader10 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader12 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.ColumnHeader13 = CType(New System.Windows.Forms.ColumnHeader(), System.Windows.Forms.ColumnHeader)
        Me.btnGetWLANList = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.ProgressBar1 = New System.Windows.Forms.ProgressBar()
        Me.GroupListView = New System.Windows.Forms.ListView()
        Me.cbListIndex = New System.Windows.Forms.ComboBox()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtStuID = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtLocation = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'lstvAvailableNetworkList
        '
        Me.lstvAvailableNetworkList.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.ColumnHeader1, Me.ColumnHeader2, Me.ColumnHeader3, Me.ColumnHeader4, Me.ColumnHeader5, Me.ColumnHeader6, Me.ColumnHeader7, Me.ColumnHeader8, Me.ColumnHeader9, Me.ColumnHeader10, Me.ColumnHeader12, Me.ColumnHeader13})
        Me.lstvAvailableNetworkList.Location = New System.Drawing.Point(12, 12)
        Me.lstvAvailableNetworkList.Name = "lstvAvailableNetworkList"
        Me.lstvAvailableNetworkList.Size = New System.Drawing.Size(995, 292)
        Me.lstvAvailableNetworkList.TabIndex = 0
        Me.lstvAvailableNetworkList.UseCompatibleStateImageBehavior = False
        Me.lstvAvailableNetworkList.View = System.Windows.Forms.View.Details
        '
        'ColumnHeader1
        '
        Me.ColumnHeader1.Text = "SSID"
        '
        'ColumnHeader2
        '
        Me.ColumnHeader2.Text = "SignalQuality"
        '
        'ColumnHeader3
        '
        Me.ColumnHeader3.Text = "dot11BssType"
        '
        'ColumnHeader4
        '
        Me.ColumnHeader4.Text = "dot11DefaultAuthAlgorithm"
        '
        'ColumnHeader5
        '
        Me.ColumnHeader5.Text = "dot11DefaultCipherAlgorithm"
        '
        'ColumnHeader6
        '
        Me.ColumnHeader6.Text = "morePhyTypes"
        '
        'ColumnHeader7
        '
        Me.ColumnHeader7.Text = "numberOfBssids"
        '
        'ColumnHeader8
        '
        Me.ColumnHeader8.Text = "networkConnectable"
        '
        'ColumnHeader9
        '
        Me.ColumnHeader9.Text = "flags"
        '
        'ColumnHeader10
        '
        Me.ColumnHeader10.Text = "profileName"
        '
        'ColumnHeader12
        '
        Me.ColumnHeader12.Text = "securityEnabled"
        '
        'ColumnHeader13
        '
        Me.ColumnHeader13.Text = "wlanNotConnectableReason"
        '
        'btnGetWLANList
        '
        Me.btnGetWLANList.Location = New System.Drawing.Point(78, 329)
        Me.btnGetWLANList.Name = "btnGetWLANList"
        Me.btnGetWLANList.Size = New System.Drawing.Size(133, 42)
        Me.btnGetWLANList.TabIndex = 1
        Me.btnGetWLANList.Text = "Get WLAN List"
        Me.btnGetWLANList.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(78, 388)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(133, 42)
        Me.Button1.TabIndex = 2
        Me.Button1.Text = "GetNetworkBSSList"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        '
        'Button2
        '
        Me.Button2.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button2.Location = New System.Drawing.Point(707, 334)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 3
        Me.Button2.Text = "Start Get"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Timer2
        '
        '
        'ProgressBar1
        '
        Me.ProgressBar1.BackColor = System.Drawing.SystemColors.Control
        Me.ProgressBar1.Location = New System.Drawing.Point(591, 374)
        Me.ProgressBar1.Name = "ProgressBar1"
        Me.ProgressBar1.Size = New System.Drawing.Size(307, 23)
        Me.ProgressBar1.TabIndex = 4
        '
        'GroupListView
        '
        Me.GroupListView.Location = New System.Drawing.Point(1032, 12)
        Me.GroupListView.Name = "GroupListView"
        Me.GroupListView.Size = New System.Drawing.Size(299, 292)
        Me.GroupListView.TabIndex = 5
        Me.GroupListView.UseCompatibleStateImageBehavior = False
        Me.GroupListView.View = System.Windows.Forms.View.Details
        '
        'cbListIndex
        '
        Me.cbListIndex.FormattingEnabled = True
        Me.cbListIndex.Location = New System.Drawing.Point(1140, 329)
        Me.cbListIndex.Name = "cbListIndex"
        Me.cbListIndex.Size = New System.Drawing.Size(90, 20)
        Me.cbListIndex.TabIndex = 6
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(692, 412)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(103, 23)
        Me.Button3.TabIndex = 7
        Me.Button3.Text = "SaveToDatabase"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(280, 348)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(41, 12)
        Me.Label1.TabIndex = 8
        Me.Label1.Text = "學號："
        '
        'txtStuID
        '
        Me.txtStuID.Location = New System.Drawing.Point(351, 341)
        Me.txtStuID.Name = "txtStuID"
        Me.txtStuID.Size = New System.Drawing.Size(134, 22)
        Me.txtStuID.TabIndex = 9
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(280, 405)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(65, 12)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "檢測地點："
        '
        'txtLocation
        '
        Me.txtLocation.Location = New System.Drawing.Point(351, 400)
        Me.txtLocation.Name = "txtLocation"
        Me.txtLocation.Size = New System.Drawing.Size(134, 22)
        Me.txtLocation.TabIndex = 11
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(1045, 334)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(89, 12)
        Me.Label3.TabIndex = 12
        Me.Label3.Text = "歷次資料列表："
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1358, 447)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.txtLocation)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.txtStuID)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.cbListIndex)
        Me.Controls.Add(Me.GroupListView)
        Me.Controls.Add(Me.ProgressBar1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.btnGetWLANList)
        Me.Controls.Add(Me.lstvAvailableNetworkList)
        Me.Name = "Form1"
        Me.Text = "WiFi"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents lstvAvailableNetworkList As ListView
    Friend WithEvents btnGetWLANList As Button
    Friend WithEvents ColumnHeader1 As ColumnHeader
    Friend WithEvents ColumnHeader2 As ColumnHeader
    Friend WithEvents ColumnHeader3 As ColumnHeader
    Friend WithEvents ColumnHeader4 As ColumnHeader
    Friend WithEvents ColumnHeader5 As ColumnHeader
    Friend WithEvents ColumnHeader6 As ColumnHeader
    Friend WithEvents ColumnHeader7 As ColumnHeader
    Friend WithEvents ColumnHeader8 As ColumnHeader
    Friend WithEvents ColumnHeader9 As ColumnHeader
    Friend WithEvents ColumnHeader10 As ColumnHeader
    Friend WithEvents ColumnHeader12 As ColumnHeader
    Friend WithEvents ColumnHeader13 As ColumnHeader
    Friend WithEvents Button1 As Button
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents ProgressBar1 As System.Windows.Forms.ProgressBar
    Friend WithEvents GroupListView As ListView
    Friend WithEvents cbListIndex As ComboBox
    Friend WithEvents Button3 As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents txtStuID As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents txtLocation As TextBox
    Friend WithEvents Label3 As Label
End Class
